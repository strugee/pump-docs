Setup on Debian "jessie" stable
-------------------------------

We're going to install pump.io directly from the git repository, use
mongodb for the database and proxy pump.io behind the nginx web server.

Install basic requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~

Install the requirements via the Debian package manager. The following
commands assume you are root or if you're using ``sudo`` you have to
prefix them with ``sudo``.

::

    apt-get install nodejs-legacy npm mongodb nginx graphicsmagick git

Install and set up pump.io
~~~~~~~~~~~~~~~~~~~~~~~~~~

Next, we create a ``pumpio`` user and group and create the needed folders:

::

    groupadd pumpio
    useradd -d /var/lib/pumpio -m -r -g pumpio pumpio

    mkdir -p /srv/pumpio/uploads
    chown -R pumpio:pumpio /srv/pumpio

    mkdir /var/log/pumpio/
    chown pumpio:pumpio /var/log/pumpio/

Now, we'll switch over to the new ``pumpio`` user and run the rest of
the commands with this account:

::

    su pumpio -s /bin/bash

Let's clone the git repository and install the node.js requirements.
Unfortunately the version of ``npm`` in Debian jessie is too old, so
we'll have to start by installing our own local copy of ``npm`` that is
newer.

::

    cd /srv/pumpio
    git clone https://github.com/e14n/pump.io.git
    cd pump.io
    npm install npm
    ./node_modules/.bin/npm install

    cd node_modules/databank
    ../.bin/npm install databank-mongodb

Finally pump.io needs a configuration file in
``/var/lib/pumpio/.pump.io.json`` with the following content (adapted to
your situation):

::

    {
        "driver":  "mongodb",
        "params": {"host":"localhost","dbname":"pumpio"},
        "hostname":  "your.pump.com",
        "address": "127.0.0.1",
        "port": 8080,
        "urlPort": 443,
        "secret":  "somerandomstringhere",
        "key":  "/path/to/your/ssl-cert.crt",
        "cert":  "/path/to/your/ssl-cert.key",
        "noweb":  false,
        "site":  "your.pump.com",
        "owner":  "Your Name",
        "ownerURL":  "http://your.site.com/",
        "nologger": false,
        "logfile": "/var/log/pumpio/pumpio.log",
        "serverUser":  "pumpio",
        "uploaddir": "/srv/pumpio/uploads",
        "debugClient": false,
        "firehose": "ofirehose.com",
        "disableRegistration": true,
        "noCDN": true,
        "requireEmail": false,
        "compress": true,
        "smtpserver": "localhost",
        "proxyWhitelist": ["avatar3.status.net", "avatar.identi.ca", "secure.gravatar.com"]
    }

In particular you need to replace ``your.pump.com`` which your actual
domain name.

Make sure that the pumpio user can access your SSL certs.

You can now try and see if it works:

::

    ./bin/pump

This starts up pump.io running on port 8080, this will be proxied
behind nginx which will serve it on port 443 (https).

Setup nginx proxying
~~~~~~~~~~~~~~~~~~~~

Create the file ``/etc/nginx/sites-available/pump`` with the following
content (adapted to your situation):

::

    upstream pumpiobackend {
      server 127.0.0.1:8080 max_fails=3;
    }

    server {
      server_name your.pump.com;
      rewrite ^ https://your.pump.com$request_uri?;
    }

    server {
      listen 443 ssl;
      server_name your.pump.com;

      ssl_certificate  /path/to/your/ssl-cert.crt;
      ssl_certificate_key  /path/to/your/ssl-cert.key;

      access_log /var/log/nginx/pumpio.access.log;
      error_log /var/log/nginx/pumpio.error.log;

      client_max_body_size 500m;

      keepalive_timeout 75 75;
      gzip_vary off;

      location / {
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Real-IP $remote_addr;
    
        proxy_redirect off;
    
        proxy_buffers 16 32k;
        proxy_cache off;
        proxy_connect_timeout 60s;
        proxy_read_timeout 60s;
        proxy_pass https://pumpiobackend;
      }
    }

You will of course have to provide your own SSL cert and put them in
the right path. Make sure that the certificate key is not world
readable!

Finally you can link the file you created to enable it (as root):

::

    cd /etc/nginx/sites-enabled
    ln -s ../sites-available/pump .

If everything is set up correctly you should now be able to restart
nginx and access the site in your web browser:

::

    systemctl restart nginx 

Before you start using your site make sure have settled on the correct
hostname. You can't switch that around later without breaking
federation badly.

Start pump.io automatically using systemd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To have pump.io start automatically just create the file
``/etc/systemd/system/pump.service`` with the following content:

::

    [Unit]
    Description=pump.io
    After=syslog.target network.target

    [Service]
    Type=simple
    User=pumpio
    Group=pumpio
    ExecStart=/srv/pumpio/pump.io/bin/pump

    [Install]
    WantedBy=multi-user.target

Now to make sure systemd starts up pump.io automatically after
rebooting the server, run:

::

   systemctl enable pump

If everything seems to be working, you're done!  Congratulations!

Upgrading pump.io
~~~~~~~~~~~~~~~~~

If you later wish to upgrade your pump.io server software, e.g. to the
latest version from the git master branch, you can do something like
the following. (Note: you might want to backup your database first
using ``mongodump``, see the migration notes below.)

First, stop pump.io and switch to the pumpio user.

::

   systemctl stop pump

   su pumpio -s /bin/bash

Next, go to the code directory and upgrade with git, e.g. pull in the
newest code from the current branch:
   
::

   cd /srv/pumpio/pump.io
   git pull

Next, you need to run ``npm install`` to upgrade dependencies. I
noticed you might have to run npm install multiple times until it
appears to be satisfied.

::
   
   ./node_modules/.bin/npm install

In some cases also the database stuff might need to be upgraded:

::
   
   cd node_modules/databank
   ../.bin/npm install

If you're unsure, especially after a big upgrade, you can always test it first directly by running ``./bin/pump`` as above. If all seems well you can Ctrl-C and start it for real.

::
   
   exit
   systemctl start pump


    
Site migration
~~~~~~~~~~~~~~

If you ever want to migrate to another server, you need to copy your
mongodb contents and the upload directory. Something like this:

::

    mongodump
    rsync -var dump new.server.com:
    rsync /srv/pumpio/uploads new.server.com:

    ssh new.server.com
    mongorestore
    mv uploads to.correct.place
